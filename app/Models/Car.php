<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;
    protected $fillable=['brand','year','price','gearbox','emissions','service','information','user_id'];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    
    public function carphotos(){
        return $this->hasMany('App\Models\CarPhoto');
    }
}
