<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $rules = [
            'brand' => 'required',
            'year' => 'required',
            'price' => 'required|integer|gt:0',
            'gearbox' => 'required',
            'emissions' => 'required',
            'files' => 'array',
            'files.*' => 'image|mimes:jpeg,jpg,bmp,png'

        ];

        return $rules;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand.required' => 'Car brand is required!',
            'year.required'  => 'Year is required!',
            'price.required' => 'Car price is required!',
            'price.integer' => 'Price must be an integer value!',
            'gearbox.required' => 'Car gearbox is required!',
            'emissions.required' => 'Car emissions is required!',
            'files.image' => 'Files uploaded must be jpeg,jpg,bmp,png type!'
        ];
    }
}
