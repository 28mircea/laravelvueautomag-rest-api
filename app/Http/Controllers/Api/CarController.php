<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Car;
use App\Models\CarPhoto;
use App\Http\Requests\UploadRequest;
use App\Http\Requests\UpdateRequest;
use Illuminate\Support\Facades\Auth;

class CarController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth')->except(['resultsearch']);
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UploadRequest $request)
    {
        $car = new Car();
        $car->brand = $request->brand;
        $car->year = $request->year;
        $car->price = $request->price;
        $car->gearbox = $request->gearbox;
        $car->emissions = $request->emissions;
        $car->service = $request->service;
        $car->information = $request->information;        
        $car->user_id = \Auth::id();
        
        $car->save();        

        if($request->hasFile('files')){
            foreach ($request->file('files') as $file){
                $ext = $file->getClientOriginalExtension();
                $filename = date('YmdHis').rand(1,99999).'.'.$ext;            
                $filepath = $file->storeAs('public/images',$filename);
                
                CarPhoto::create([
                    'car_id' => $car->id,
                    'image' => $filename
                ]);
            }
        } 

        return $car;
    }


    public function mycars()    
    {
        
        $cars = Car::with('carphotos')->where('user_id','=', auth('sanctum')->user()->id)->paginate(2);      

        foreach($cars as $key => $car){
            $cars[$key]->carphotos = $car->carphotos->map(function($carphoto)
                        {
                            $carphoto->image = asset('/storage/images/'.$carphoto->image);
                            return $carphoto;
                        });
        }

        return $cars;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        $car->load('carphotos');        

        foreach($car->carphotos as $key =>$carphoto){
            $carphoto->image = asset('/storage/images/'.$carphoto->image);            
        }
       
        return $car;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //public function update(Request $request, Car $car)
    //{
        //
    //}

    public function updatecar(UpdateRequest $request, Car $car)
    {
        $car->brand = $request->brand; 
        $car->year = $request->year;     
        $car->price = $request->price;
        $car->gearbox = $request->gearbox;
        $car->emissions = $request->emissions;
        $car->service = $request->service;
        $car->information = $request->information;        

        $car->save();
       

        if($request->hasFile('files')){
            foreach ($request->file('files') as $file){
                $ext = $file->getClientOriginalExtension();
                $filename = date('YmdHis').rand(1,99999).'.'.$ext;            
                $filepath = $file->storeAs('public/images',$filename);
                
                CarPhoto::create([
                    'car_id' => $car->id,
                    'image' => $filename
                ]);
            }
        } 

        return $car;

    }

    public function destroyonephoto(Carphoto $carphoto)
    {              
        Storage::delete('/public/images/'.$carphoto->image);       
        $carphoto->delete();        
        return $carphoto; 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        foreach($car->carphotos as $carphoto){
            Storage::delete('/public/images/'.$carphoto->image);
        }

        $car->delete();
        return $car;    
    }

    public function resultsearch(Request $request){
        
        $brand = $request->input('brand');
        $words = explode(" ", $brand);

        $startyear = $request->input('startyear');       
        $endyear = $request->input('endyear');

        $startprice = $request->input('startprice');       
        $endprice = $request->input('endprice');

        $gearbox = $request->input('gearbox');
        $emissions = $request->input('emissions');
        $service = $request->input('service');

        $query = Car::with(['carphotos','user'])->where('id','>', 0);

        

        foreach($words as $word){
            $query->where('brand', 'like', "%$word%");
        }

        if($startyear && $endyear){
            $query->whereBetween('year', [$startyear, $endyear]);
        }

        if($startprice && $endprice){
            $query->whereBetween('price', [$startprice, $endprice]);
        }
        
        if($gearbox){
            $query->where('gearbox','like',"%$gearbox%");
        }
        
        if($emissions){
            $query->where('emissions','like',"%$emissions%");
        }
        
        if($service){
            $query->where('service','like',"%$service%");
        }
        
        if('asc' == $request->input('filter')){
            $query->orderBy('price', 'ASC');
        }
        
        if('desc' == $request->input('filter')){
            $query->orderBy('price','DESC');
        }

        if('newest' == $request->input('filter')){
            $query->orderBy('created_at','DESC');
        }

        if('oldest' == $request->input('filter')){
            $query->orderBy('created_at','ASC');
        }
        
        $cars = $query->get();


        foreach($cars as $key => $q){
            $cars[$key]->carphotos = $q->carphotos->map(function($carphoto)
                        {
                            $carphoto->image = asset('/storage/images/'.$carphoto->image);
                            return $carphoto;
                        });
        }
                      
        return $cars;  
    }
}
