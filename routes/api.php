<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CarController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::post('logout', [AuthController::class, 'logout'])->middleware('auth:sanctum');

Route::get('users', [AuthController::class, 'index'])->middleware('auth:sanctum');
Route::get('edit/{user}', [AuthController::class, 'show'])->middleware('auth:sanctum');
Route::put('update/{user}', [AuthController::class, 'update'])->middleware('auth:sanctum');
Route::delete('delete/{user}', [AuthController::class, 'destroy'])->middleware('auth:sanctum');

Route::get('/mycars', [CarController::class, 'mycars'])->middleware('auth:sanctum');
Route::delete('/destroy-onephoto/{carphoto}', [CarController::class, 'destroyonephoto'])->middleware('auth:sanctum');
Route::post('/updatecar/{car}', [CarController::class, 'updatecar'])->middleware('auth:sanctum');
Route::post('/search', [CarController::class, 'resultsearch']);
Route::resource('cars', CarController::class)->middleware('auth:sanctum');
