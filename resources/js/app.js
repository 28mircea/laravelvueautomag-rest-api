require('./bootstrap');

import Vue from 'vue';
import App from './App.vue';
import {routes} from './routes';
window.Vue = require('vue').default;

import VueRouter from 'vue-router';
Vue.use(VueRouter);


const router = new VueRouter({
    mode:'history',
    routes: routes
});

function loggedIn(){
    return localStorage.getItem('token')
}

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!loggedIn()) {
            next({
            path: '/login',
            query: { redirect: to.fullPath }
            })
        } else {
            next()
        }
    } else if(to.matched.some(record => record.meta.guest)) {
        if (loggedIn()) {
            next({
            path: '/users',
            query: { redirect: to.fullPath }
            })
        } else {
            next()
        }
    } else {
        next() // make sure to always call next()!
    }
})

Vue.component('pagination', require('laravel-vue-pagination'));
//Vue.component('example-component', require('./components/ExampleComponent.vue').default);


const app = new Vue({
    el: '#app',
    router: router,
    render: h => h(App),
});
