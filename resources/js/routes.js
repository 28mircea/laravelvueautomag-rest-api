import LoginComponent from './components/auth/LoginComponent.vue';
import RegisterComponent from './components/auth/RegisterComponent.vue';
import UsersComponent from './components/users/UsersComponent.vue';
import EditComponent from './components/users/EditComponent';
import AddCar from './components/cars/AddCar';
import EditCar from './components/cars/EditCar';
import SearchCar from './components/cars/SearchCar';

 
export const routes = [
    {
        name:'home',
        path:'/'
        
    },
    {
        name: 'login',
        path: '/login',
        component: LoginComponent
    },
    {
        name: 'register',
        path: '/register',
        component: RegisterComponent
    },
    {
        name: 'users',
        path: '/users',
        component: UsersComponent
        
    },    
    {
        name: 'edit',
        path: '/edit/:id?',
        component: EditComponent
    },
    {
        name: 'addcar',
        path: '/addcar',
        component: AddCar
    },
    {
        name: 'editcar',
        path: '/editcar/:id?',
        component: EditCar
    },
    {
        name: 'searchcar',
        path: '/searchcar',
        component: SearchCar
    }   
   
];



